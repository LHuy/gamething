package games.treasurehunt2015;

import event.TreasureCrashEvent;
import graphicslib3D.Point3D;
import graphicslib3D.Vector3D;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import myGameEngine.CustomTreasure;
import myGameEngine.MyDiamond;
import myGameEngine.MyDisplaySystem;
import myGameEngine.Camera.Camera3Pcontroller;
import myGameEngine.Camera.input.Cam3pLeft;
import myGameEngine.Camera.input.Cam3pRight;
import myGameEngine.Camera.input.MoveBackAction3p;
import myGameEngine.Camera.input.MoveForwardAction3P;
import myGameEngine.Camera.input.MoveLeftAction3p;
import myGameEngine.Camera.input.MoveRightAction3p;
import myGameEngine.Camera.input.ZoomIn3p;
import myGameEngine.Camera.input.ZoomOut3p;
import myGameEngine.controller.MyBounceController;
import myGameEngine.controller.MyDisapearController;
import myGameEngine.controller.MyTranslateController;
import myGameEngine.input.action.MyQuitGameAction;
import myGameEngine.input.action.MyRXaxis;
import myGameEngine.input.action.MyRYaxis;
import myGameEngine.input.action.MyXaxis;
import myGameEngine.input.action.MyYaxis;
import myGameEngine.input.action.SetSpeedAction;
import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Version;
import sage.app.BaseGame;
import sage.camera.JOGLCamera;
import sage.display.DisplaySystem;
import sage.display.IDisplaySystem;
import sage.event.EventManager;
import sage.event.IEventManager;
import sage.input.IInputManager;
import sage.input.InputManager;
import sage.renderer.IRenderer;
import sage.scene.HUDString;
import sage.scene.SceneNode;
import sage.scene.shape.Cube;
import sage.scene.shape.Cylinder;
import sage.scene.shape.Line;
import sage.scene.shape.Pyramid;
import sage.scene.shape.Rectangle;
import sage.scene.shape.Sphere;

public class GameBase extends BaseGame {

  public GameBase() {
    listController();
    start(); // start calls initsystem , initgame,update

  }

  private float time = 0;

  private int treasureCollected;
  private HUDString timeString;

  // private IDisplaySystem display;

  private IEventManager eventManager;

  boolean disableFlag = true;
  Random randomGenerator = new Random();

  private IRenderer renderer;
  private SceneNode player1, player2;
  private sage.camera.ICamera camera;
  private sage.camera.ICamera camera2;
  private Camera3Pcontroller cam1Controller;
  private Camera3Pcontroller cam2Controller;

  private IDisplaySystem createDisplaySystem() {
    
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    IDisplaySystem display = new MyDisplaySystem((int) screenSize.getWidth(), (int) screenSize.getHeight(), 32, 60, true,
        "sage.renderer.jogl.JOGLRenderer");
    System.out.print("\nWaiting for display creation...");
    int count = 0;
    // wait until display creation completes or a timeout occurs
    while (!display.isCreated()) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        throw new RuntimeException("Display creation interrupted");
      }
      count++;
      System.out.print("+");
      if (count % 80 == 0) {
        System.out.println();
      }
      if (count > 2000) // 20 seconds (approx.)
      {
        throw new RuntimeException("Unable to create display");
      }
    }
    System.out.println();
    return display;
  }

  @Override
  protected void initSystem() {
    // call a local method to create a DisplaySystem object
  
    IDisplaySystem display =getDisplaySystem();
    display = createDisplaySystem();
    setDisplaySystem(display);

    // create an Input Manager
    IInputManager inputManager = new InputManager();
    setInputManager(inputManager);

    // create an (empty) gameworld
    ArrayList<SceneNode> gameWorld = new ArrayList<SceneNode>();
    setGameWorld(gameWorld);
    
  super.initSystem();
   
  }

  public void initGame() {

    renderer = getDisplaySystem().getRenderer();

    createPlayers();

    // event manager
    eventManager = EventManager.getInstance();
    
    createScene();
    
    // Configure game display
    getDisplaySystem().setTitle("TEAM Treasure Hunt 2015 By Huy Le");

    mapInputKeys();
 
  }

  private void createScene() {
    // Initalize Group treasures
    sage.scene.Group treasures;

    Sphere groupTreasure1 = new Sphere(1, 20, 20, Color.WHITE);
    Sphere groupTreasure2 = new Sphere(1, 20, 20, Color.BLUE);
    groupTreasure1.translate(0, 3, 0);

    treasures = new sage.scene.Group("root");
    treasures.addChild(groupTreasure1);
    treasures.addChild(groupTreasure2);
    treasures.translate(12, 2, 0);
    addGameWorldObject(treasures);

    //Controller for group
    MyTranslateController mtc = new MyTranslateController();
    mtc.addControlledNode(treasures);
    treasures.addController(mtc);

    // Create Lines 
    Line XAxisLine = new Line(new Point3D(-50, 0, 0), new Point3D(50, 0, 0),
        Color.red, 3);
    Line YAxisLine = new Line(new Point3D(0, -50, 0), new Point3D(0, 50, 0),
        Color.blue, 3);
    Line ZAxisLine = new Line(new Point3D(0, 0, -50), new Point3D(0, 0, 50),
        Color.GREEN, 3);
    addGameWorldObject(XAxisLine);
    addGameWorldObject(YAxisLine);
    addGameWorldObject(ZAxisLine);

    //Create Treasure box
    CustomTreasure treasureChest = new CustomTreasure();
    float tx = 2.0f;
    float ty = 5.0f;

    treasureChest.translate(tx, ty, 0.0f);
    treasureChest.scale(0.2f, 0.2f, 0.2f);

    addGameWorldObject(treasureChest);
    eventManager.addListener(treasureChest, TreasureCrashEvent.class);

   //Create diamond
    MyDiamond prMyPyramid = new MyDiamond();

    float txp = randomGenerator.nextInt(9);
    float typ = randomGenerator.nextInt(2);
    float tzp = randomGenerator.nextInt(9);

    prMyPyramid.translate(txp, typ, tzp);
    addGameWorldObject(prMyPyramid);

    //Create Cylinder
    Cylinder cylinderTreasure = new Cylinder();
    addGameWorldObject(cylinderTreasure);
    
    
    Sphere sphereTreasure = new Sphere();
    tx = randomGenerator.nextInt(10);
    ty = randomGenerator.nextInt(2);
    float tz = randomGenerator.nextInt(10);

    sphereTreasure.translate(tx, ty, tz);
    addGameWorldObject(sphereTreasure);

    MyDisapearController disapearController = new MyDisapearController();
    disapearController.addControlledNode(sphereTreasure);
    sphereTreasure.addController(disapearController);

    Sphere sphereTreasure2 = new Sphere();
    tx = randomGenerator.nextInt(10);
    ty = randomGenerator.nextInt(2);
    tz = randomGenerator.nextInt(10);

    sphereTreasure2.translate(tx, ty, tz);
    addGameWorldObject(sphereTreasure2);

    MyBounceController bounceController = new MyBounceController();
    bounceController.addControlledNode(sphereTreasure2);
    sphereTreasure2.addController(bounceController);

    // Ground plane
    Rectangle groundPlane = new Rectangle(1, 1);
    groundPlane.setColor(Color.ORANGE);
    groundPlane.rotate(90, new Vector3D(1, 0, 0));
    groundPlane.scale(10000, 10000, 1);
    addGameWorldObject(groundPlane);
    
  }

  private void createPlayers() {
    player1 = new Pyramid("Player1");
    player1.translate(0, 1, 50);
    player1.rotate(180, new Vector3D(0, 1, 0));
    addGameWorldObject(player1);

    camera = new JOGLCamera(renderer);
    camera.setPerspectiveFrustum(60, 2, 1, 1000);
    camera.setViewport(0.0, 1.0, 0.0, 0.45);

    player2 = new Cube("Player2");
    player2.translate(5, 1, 0);
    player2.rotate(-90, new Vector3D(0, 1, 0));
    addGameWorldObject(player2);

    camera2 = new JOGLCamera(renderer);
    camera2.setPerspectiveFrustum(60, 2, 1, 1000);
    camera2.setViewport(0.0, 1.0, 0.55, 1.0);

    createPlayerHUDs();

  }

  private void createPlayerHUDs() {
    HUDString player1ID = new HUDString("Player1");
    player1ID.setName("Player1ID");
    player1ID.setLocation(0.01, 0.06);
    player1ID.setRenderMode(sage.scene.SceneNode.RENDER_MODE.ORTHO);
    player1ID.setColor(Color.RED);
    player1ID.setCullMode(sage.scene.SceneNode.CULL_MODE.NEVER);
    camera.addToHUD(player1ID);

    timeString = new HUDString("Time = " + time);
    timeString.setLocation(0, 1.15); // (0,0) [lower-left] to (1,1)
    camera.addToHUD(timeString);

    HUDString player2ID = new HUDString("Player2");
    player2ID.setName("Player2ID");
    player2ID.setLocation(0.01, 0.06);
    player2ID.setRenderMode(sage.scene.SceneNode.RENDER_MODE.ORTHO);
    player2ID.setColor(Color.YELLOW);
    player2ID.setCullMode(sage.scene.SceneNode.CULL_MODE.NEVER);
    camera2.addToHUD(player2ID);
  }

  protected void render() {
    renderer.setCamera(camera);
    super.render();

    renderer.setCamera(camera2);
    super.render();

  }

  public void update(float elapsedTimeMS) {

    cam1Controller.update(elapsedTimeMS);
    cam2Controller.update(elapsedTimeMS);
    super.update(elapsedTimeMS);

    SceneNode storeNode = null;

    for (SceneNode node : this.getGameWorld())
      if (node instanceof Cylinder) {
        if (node.getWorldBound().intersects(player1.getWorldBound()) || node.getWorldBound().intersects(player2.getWorldBound())) {
          System.out.println("Cylinder collision");
          treasureCollected++;
          storeNode = node;
          TreasureCrashEvent treasureCrashEvent = new TreasureCrashEvent(
              treasureCollected);
          eventManager.triggerEvent(treasureCrashEvent);
          eventManager.abortEvent(TreasureCrashEvent.class, true);
        }

      }
    removeGameWorldObject(storeNode);

    for (SceneNode node : this.getGameWorld())
      if (node instanceof Sphere) {
        if (node.getWorldBound().intersects(player1.getWorldBound()) || node.getWorldBound().intersects(player2.getWorldBound())) {
          System.out.println("Sphere collision");
          treasureCollected++;
          storeNode = node;
          TreasureCrashEvent treasureCrashEvent = new TreasureCrashEvent(
              treasureCollected);
          eventManager.triggerEvent(treasureCrashEvent);
          eventManager.abortEvent(TreasureCrashEvent.class, true);
        }

      }
    removeGameWorldObject(storeNode);

    for (SceneNode node : this.getGameWorld())
      if (node instanceof MyDiamond) {
        if (node.getWorldBound().intersects(player1.getWorldBound()) || node.getWorldBound().intersects(player2.getWorldBound())) {
          System.out.println("Sphere collision");
          treasureCollected++;
          storeNode = node;
          TreasureCrashEvent treasureCrashEvent = new TreasureCrashEvent(
              treasureCollected);
          eventManager.triggerEvent(treasureCrashEvent);
          eventManager.abortEvent(TreasureCrashEvent.class, true);
        }

      }
    removeGameWorldObject(storeNode);

    for (SceneNode node : this.getGameWorld())
      if (node instanceof sage.scene.Group) {
        if (node.getWorldBound().intersects(player1.getWorldBound()) || node.getWorldBound().intersects(player2.getWorldBound()) ) {
          System.out.println("Group collision");
          storeNode = node;
          treasureCollected++;
          TreasureCrashEvent treasureCrashEvent = new TreasureCrashEvent(
              treasureCollected);
          eventManager.triggerEvent(treasureCrashEvent);
          eventManager.abortEvent(TreasureCrashEvent.class, true);

        }

      }
    removeGameWorldObject(storeNode);

    time += elapsedTimeMS;
    DecimalFormat df = new DecimalFormat("0.0");
    timeString.setText("Time = " + df.format(time / 1000));

  }

  public void mapInputKeys() {
    SetSpeedAction setSpeedAction = new SetSpeedAction();
    IInputManager im = getInputManager();

    String kbName = im.getKeyboardName();

    if (im.getFirstGamepadName() != null) {
      mapGamepadKeys(im, setSpeedAction);
    } else {
      System.out.println("GAMEPAD DISABLED KEYBOARD ONLY ACTIVE");
    }

    // 3p Commands start
    /*
 
    */

    cam1Controller = new Camera3Pcontroller(camera, player1, im, kbName);
    cam2Controller = new Camera3Pcontroller(camera2, player2, im, kbName);
    // Move players 
    MoveForwardAction3P moveForwardAction3P = new MoveForwardAction3P(player1);
    MoveForwardAction3P moveForwardAction3P2 = new MoveForwardAction3P(player2);
    MoveBackAction3p moveBackAction3p = new MoveBackAction3p(player1);
    MoveBackAction3p moveBackAction3p2 = new MoveBackAction3p(player2);
    MoveLeftAction3p moveLeftAction3p = new MoveLeftAction3p(player1);
    MoveLeftAction3p moveLeftAction3p2 = new MoveLeftAction3p(player2);
    MoveRightAction3p moveRightAction3p = new MoveRightAction3p(player1);
    MoveRightAction3p moveRightAction3p2 = new MoveRightAction3p(player2);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.D,
        moveRightAction3p, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName,
        net.java.games.input.Component.Identifier.Key.DOWN, moveRightAction3p2,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.A,
        moveLeftAction3p, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName,
        net.java.games.input.Component.Identifier.Key.UP, moveLeftAction3p2,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.W,
        moveForwardAction3P, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName,
        net.java.games.input.Component.Identifier.Key.RIGHT,
        moveForwardAction3P2, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.S,
        moveBackAction3p, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName,
        net.java.games.input.Component.Identifier.Key.LEFT, moveBackAction3p2,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    
    
    //Zoom orbit camera 
    ZoomIn3p zoomIn3p = new ZoomIn3p(cam1Controller);
    ZoomIn3p zoomIn3p2 = new ZoomIn3p(cam2Controller);
    ZoomOut3p zoomOut3p = new ZoomOut3p(cam1Controller);
    ZoomOut3p zoomOut3p2 = new ZoomOut3p(cam2Controller);
    
    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.Q,
        zoomIn3p, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName,
        net.java.games.input.Component.Identifier.Key.NUMPAD1, zoomIn3p2,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.E,
        zoomOut3p, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName,
        net.java.games.input.Component.Identifier.Key.NUMPAD2, zoomOut3p2,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
    
    //Azimuth
    Cam3pLeft cam3pLeft = new Cam3pLeft(cam1Controller);
    Cam3pRight cam3pRight = new Cam3pRight(cam1Controller);
    Cam3pLeft cam3pLeftp2 = new Cam3pLeft(cam2Controller);
    Cam3pRight cam3pRightp2 = new Cam3pRight(cam2Controller);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.Z,
        cam3pLeft, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.C,
        cam3pRight, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.NUMPAD4,
        cam3pLeftp2, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.NUMPAD5,
        cam3pRightp2, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    // 3pCommands end

    MyQuitGameAction myQuitGameAction = new MyQuitGameAction(this);

    im.associateAction(kbName,
        net.java.games.input.Component.Identifier.Key.ESCAPE, myQuitGameAction,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    // Controller Camera rotations

  }

  public void mapGamepadKeys(IInputManager im, SetSpeedAction setSpeedAction) {
    String contName = im.getFirstGamepadName();

    MyRYaxis myRYaxis = new MyRYaxis(camera);
    MyRXaxis myRXaxis = new MyRXaxis(camera);

    im.associateAction(contName,
        net.java.games.input.Component.Identifier.Axis.RY, myRYaxis,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(contName,
        net.java.games.input.Component.Identifier.Axis.RX, myRXaxis,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    MyYaxis myYaxis = new MyYaxis(camera, setSpeedAction);
    MyXaxis myXaxis = new MyXaxis(camera, setSpeedAction);

    im.associateAction(contName,
        net.java.games.input.Component.Identifier.Axis.Y, myYaxis,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

    im.associateAction(contName,
        net.java.games.input.Component.Identifier.Axis.X, myXaxis,
        IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

  }

  // Shows all the buttons or inputs from controllers
  public void listComponenets(Controller controller) {
    System.out.println("Name: " + controller.getName() + ". Type ID:"
        + controller.getType());

    Component[] comps = controller.getComponents();

    for (Component component : comps) {
      System.out.println(" Name: " + component.getName() + " ID:"
          + component.getIdentifier());

    }

    Controller[] subControllers = controller.getControllers();
    for (Controller subController : subControllers) {
      System.out.println(" " + controller.getName() + "subController #"
          + controller.getPortNumber());
      listComponenets(subController);
    }

  }

  public void listController() {
    System.out.println("Jinput version" + Version.getVersion());
    ControllerEnvironment ce = ControllerEnvironment.getDefaultEnvironment();
    System.out.println(ce);
    Controller[] cs = ce.getControllers();

    for (Controller controller : cs) {
      System.out.println("\nController #" + controller.getPortNumber());
      listComponenets(controller);

    }

  }

  public void respawnCylinder() {
    Cylinder cylinderTreasure = new Cylinder();
    addGameWorldObject(cylinderTreasure);
  }

  public void respawnSphere() {

  }

  public void shutdown() {
    super.shutdown();
    getDisplaySystem().close();
  }


  
}

/*
 * // Camera Movement
 * 
 * MyMoveUpAction myMoveUpAction = new MyMoveUpAction(camera, setSpeedAction);
 * MyMoveDownAction myMoveDownAction = new MyMoveDownAction(camera,
 * setSpeedAction);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.Q,
 * myMoveDownAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.E,
 * myMoveUpAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * MyMoveRightAction myMoveRightAction = new MyMoveRightAction(camera,
 * setSpeedAction); MyMoveLeftAction myMoveLeftAction = new
 * MyMoveLeftAction(camera, setSpeedAction); MyMoveBackAction MyMoveBackAction =
 * new MyMoveBackAction(camera, setSpeedAction); MyMoveForwardAction
 * MyMoveForwardAction = new MyMoveForwardAction(camera, setSpeedAction);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.W,
 * MyMoveForwardAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.S,
 * MyMoveBackAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.A,
 * myMoveRightAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.D,
 * myMoveLeftAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * // Controller Movement Actions
 * 
 * // Camera rotations MyLookUpAction mylookUpAction = new
 * MyLookUpAction(camera); MyLookDownAction mylookDownAction = new
 * MyLookDownAction(camera); MyLookLeftAction mylookLeftAction = new
 * MyLookLeftAction(camera); MyLookRightAction mylookRightAction = new
 * MyLookRightAction(camera);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.UP,
 * mylookUpAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName,
 * net.java.games.input.Component.Identifier.Key.DOWN, mylookDownAction,
 * IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName,
 * net.java.games.input.Component.Identifier.Key.LEFT, mylookLeftAction,
 * IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName,
 * net.java.games.input.Component.Identifier.Key.RIGHT, mylookRightAction,
 * IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * ResetCamera resetCamera = new ResetCamera(camera);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.R,
 * resetCamera, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * 
 * 
 * 
 * 
 * public void sageMapInputKeys() {
 * 
 * 
 * String kbName = im.getKeyboardName();
 * 
 * MyQuitGameAction myQuitGameAction = new MyQuitGameAction(this);
 * 
 * im.associateAction(kbName,
 * net.java.games.input.Component.Identifier.Key.ESCAPE, myQuitGameAction,
 * IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * // Camera view actions (does not move camera) LookDownAction lookDownAction =
 * new LookDownAction(camera, speed); LookLeftAction lookLeftAction = new
 * LookLeftAction(camera, speed); LookRightAction lookRightAction = new
 * LookRightAction(camera, speed); LookUpAction lookUpAction = new
 * LookUpAction(camera, speed);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.M,
 * lookDownAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.N,
 * lookLeftAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.B,
 * lookRightAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.V,
 * lookUpAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * SetSpeedAction setSpeedAction = new SetSpeedAction();
 * 
 * 
 * 
 * // Moving the camera actions MoveBackwardAction moveBackwardAction = new
 * MoveBackwardAction(camera, speed, null, false); MoveForwardAction
 * moveForwardAction = new MoveForwardAction(camera, speed, null, false);
 * MoveUpAction moveUpAction = new MoveUpAction(camera, speed, runAction);
 * MoveDownAction moveDownAction = new MoveDownAction(camera, speed, runAction);
 * MoveLeftAction moveLeftAction = new MoveLeftAction(camera, speed, null);
 * MoveRightAction moveRightAction = new MoveRightAction(camera, speed, null);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.U,
 * moveUpAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.J,
 * moveDownAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.Y,
 * moveBackwardAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.I,
 * moveForwardAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.H,
 * moveLeftAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * im.associateAction(kbName, net.java.games.input.Component.Identifier.Key.K,
 * moveRightAction, IInputManager.INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);
 * 
 * }
 * 
 * // update hud /* scoreString.setText("Score =" + score);
 * 
 * 
 * worldLocationString .setText("World Loc: " +
 * camera.getLocation().toString());
 * treasureCountString.setText("Treasure Count: " + treasureCollected);
 */

// Treasure chest collision
// SceneNode storeNode = null;
/*
 * for (SceneNode node : this.getGameWorld()) { if (node instanceof Cube) { if
 * (node.getWorldBound().contains(camera.getLocation())) { treasureCollected++;
 * TreasureCrashEvent treasureCrashEvent = new TreasureCrashEvent(
 * treasureCollected); eventManager.triggerEvent(treasureCrashEvent);
 * eventManager.abortEvent(TreasureCrashEvent.class, true); storeNode = node; }
 * this.removeGameWorldObject(storeNode); }
 */
/*
 * for (SceneNode node : this.getGameWorld()) { if (node instanceof Cylinder) {
 * if (node.getWorldBound().contains(camera.getLocation())) {
 * treasureCollected++; TreasureCrashEvent treasureCrashEvent = new
 * TreasureCrashEvent( treasureCollected);
 * eventManager.triggerEvent(treasureCrashEvent);
 * eventManager.abortEvent(TreasureCrashEvent.class, true);
 * 
 * storeNode = node; }
 * 
 * }
 * 
 * }
 * 
 * 
 * if (storeNode instanceof Cylinder) { removeGameWorldObject(storeNode);
 * 
 * }
 * 
 * // addGameWorldObject(storeNode); //
 * eventManager.removeListener(treasureChest, TreasureCrashEvent.class);
 * 
 * for (SceneNode node : this.getGameWorld()) { if (node instanceof Sphere) { if
 * (node.getWorldBound().contains(camera.getLocation())) { treasureCollected++;
 * TreasureCrashEvent treasureCrashEvent = new TreasureCrashEvent(
 * treasureCollected); eventManager.triggerEvent(treasureCrashEvent);
 * eventManager.abortEvent(TreasureCrashEvent.class, true);
 * 
 * storeNode = node; } } }
 * 
 * removeGameWorldObject(storeNode);
 * 
 * for (SceneNode node : this.getGameWorld()) { if (node instanceof MyDiamond) {
 * if (node.getWorldBound().contains(camera.getLocation())) {
 * treasureCollected++; TreasureCrashEvent treasureCrashEvent = new
 * TreasureCrashEvent( treasureCollected);
 * eventManager.triggerEvent(treasureCrashEvent);
 * eventManager.abortEvent(TreasureCrashEvent.class, true);
 * 
 * storeNode = node; } } }
 * 
 * removeGameWorldObject(storeNode);
 */

// cc.update(elapsedTimeMS);

