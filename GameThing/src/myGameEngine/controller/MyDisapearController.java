package myGameEngine.controller;
import graphicslib3D.Matrix3D;

import org.luaj.vm2.lib.PackageLib.loadlib;

import com.jogamp.graph.curve.opengl.Renderer;

import sage.physics.ODE4J.ODE4JBoxObject;
import sage.scene.Controller;
import sage.scene.SceneNode;
import sage.scene.SceneNode.RENDER_MODE;

public class MyDisapearController extends Controller {

  private double translateRate = .02; 
  private double cycleTime = 2000.0;
  private double totalTime; 
  private double direction = 1.0; 
  
  public void setCycleTime(double c) {
    cycleTime = c;
  }
   
  @Override
  public void update(double time) {
    totalTime+= time; 
 if(totalTime > 10000){
    for(SceneNode node :controlledNodes){
      Matrix3D curTrans = node.getLocalTranslation();
      curTrans.scale(0, 0, 0);
      node.setLocalTranslation(curTrans);
      
    }
 }
    
  }

}
