package myGameEngine.controller;



import graphicslib3D.Matrix3D;

import org.luaj.vm2.lib.PackageLib.loadlib;

import sage.physics.ODE4J.ODE4JBoxObject;
import sage.scene.Controller;
import sage.scene.SceneNode;

public class MyBounceController extends Controller {

  private double translateRate = .02; 
  private double cycleTime = 2000.0;
  private double totalTime; 
  private double direction = 1.0; 
  
  public void setCycleTime(double c) {
    cycleTime = c;
  }
   
  @Override
  public void update(double time) {
    totalTime+= time; 
    double transAmount = translateRate * time;
    
    if(totalTime >cycleTime){
      direction = -direction; 
      totalTime = 0.0;
    }
    
    transAmount = direction * transAmount;
    
    Matrix3D newTrans = new Matrix3D();
    newTrans.translate(0, transAmount, 0);

    for(SceneNode node :controlledNodes){
      Matrix3D curTrans = node.getLocalTranslation();
      curTrans.concatenate(newTrans); 
      node.setLocalTranslation(curTrans);
      
      
    }
    
    
  }

}
