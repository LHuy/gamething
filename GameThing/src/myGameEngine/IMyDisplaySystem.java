package myGameEngine;

import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseListener;

import sage.renderer.IRenderer;

public interface IMyDisplaySystem {

  int getWidth();
  int setWidth(int width);
  
  int getHeight();
  int setHeight(int height);
  
  int getBitDepth(); 
  int setBitDepth(int bitDepth);
  
  int getRefreshRate();
  int setRefreshRate(int refreshRate);
  
  void setTitle(String title);
  
  IRenderer getRenderer();
  
  boolean isCreated();
  
  boolean isFullScreen();
  
  void addKeyListener(KeyListener k);
  void addMouseListner(MouseListener m);
  
  void setPredefinedCursor(int cursor);
  void setCustomCursor(String fileName);
  
  
  
}
