package myGameEngine;

import java.awt.Canvas;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;

import com.jogamp.newt.Window;

import sage.display.DisplaySystem;
import sage.display.IDisplaySystem;
import sage.renderer.IRenderer;
import sage.renderer.RendererFactory;
import sage.scene.state.jogl.JOGLTextureState;

public class MyDisplaySystem implements IDisplaySystem  {
  
  
  private int width,refreshRate,height,bitDepth;
  private boolean isFullScreen;
  private IRenderer myRenderer;
  private Canvas rendererCanvas;
  private JFrame myFrame;
  private boolean isCreated;
  private GraphicsDevice device; 

  public MyDisplaySystem(int w, int h, int depth, int rate, boolean isFS,
                          String rName){
    width = w; 
    height = h;  
    bitDepth = depth; 
    refreshRate = rate; 
    isFullScreen = isFS;
    
    //Create render with factory
    myRenderer = RendererFactory.createRenderer(rName);
    
    if(myRenderer == null){
      throw new RuntimeException("Unable to find renderer");
    }
    
    
    rendererCanvas = myRenderer.getCanvas();
    myFrame = new JFrame("Default Title");
    //Put render into the Jframe
    myFrame.add(rendererCanvas);
    
    //initalize screen with specific parameter
    DisplayMode displayMode = new DisplayMode(width, height, bitDepth, refreshRate);
    initScreen(displayMode,isFullScreen);
    
    //save displayscreen, show frame and indicate Displayt system
    DisplaySystem.setCurrentDisplaySystem(this);
    myFrame.setVisible(true);
    isCreated = true; 

  }
  


  private void initScreen(DisplayMode dispMode, boolean FSRequested) {
  
    GraphicsEnvironment enviorment = GraphicsEnvironment.getLocalGraphicsEnvironment();
    device = enviorment.getDefaultScreenDevice();
    
    if(device.isFullScreenSupported() && FSRequested){
      myFrame.setUndecorated(true);
      myFrame.setResizable(false);
      myFrame.setIgnoreRepaint(true);
      
      //put device in full screen
      device.setFullScreenWindow(myFrame);
      
      if(dispMode != null && device.isDisplayChangeSupported()){
        try{
          device.setDisplayMode(dispMode);
          myFrame.setSize(dispMode.getWidth(), dispMode.getHeight());
          
        }catch(Exception ex){
          System.err.println("Ecception setting Display mode:" + ex);
        }
        
      }else {
        System.err.println("Cannot Set display mode");
      }
    }else {
      myFrame.setSize(dispMode.getWidth(), dispMode.getHeight());
      myFrame.setLocationRelativeTo(null);
    }
    
    
    
    
  }



  @Override
  public void addKeyListener(KeyListener arg0) {
    // TODO Auto-generated method stub
    
  }



  @Override
  public void addMouseListener(MouseListener arg0) {
    // TODO Auto-generated method stub
    
  }



  @Override
  public void addMouseMotionListener(MouseMotionListener arg0) {
    // TODO Auto-generated method stub
    
  }



  @Override
  public void close() {
   if(device != null){
     Window window = (Window) device.getFullScreenWindow();
     if(window != null){
       ((java.awt.Window) window).dispose();
     }
     device.setFullScreenWindow(null);
   }
    
  }



  @Override
  public void convertPointToScreen(Point arg0) {
    // TODO Auto-generated method stub
    
  }



  @Override
  public int getBitDepth() {
    // TODO Auto-generated method stub
    return this.bitDepth;
  }



  @Override
  public int getHeight() {
    // TODO Auto-generated method stub
    return this.height;
  }



  @Override
  public int getRefreshRate() {
    // TODO Auto-generated method stub
    return this.refreshRate;
  }



  @Override
  public IRenderer getRenderer() {
    // TODO Auto-generated method stub
    return myRenderer;
  }



  @Override
  public int getWidth() {
    // TODO Auto-generated method stub
    return this.width;
  }



  @Override
  public boolean isCreated() {
    // TODO Auto-generated method stub
    return this.isCreated;
  }



  @Override
  public boolean isFullScreen() {
    // TODO Auto-generated method stub
    return this.isFullScreen;
  }



  @Override
  public boolean isShowing() {
    // TODO Auto-generated method stub
    return false;
  }



  @Override
  public void setBitDepth(int arg0) {
    bitDepth=arg0;
    
  }



  @Override
  public void setCustomCursor(String arg0) {
    // TODO Auto-generated method stub
    
  }



  @Override
  public void setHeight(int arg0) {
   height=arg0;
    
  }



  @Override
  public void setPredefinedCursor(int arg0) {
    // TODO Auto-generated method stub
    
  }



  @Override
  public void setRefreshRate(int arg0) {
   refreshRate = arg0;
    
  }



  @Override
  public void setTitle(String arg0) {
  
    
  }



  @Override
  public void setWidth(int arg0) {
   width = arg0;
    
  }
  
  
  
  
  
}
