package myGameEngine.input.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import graphicslib3D.Point3D;
import graphicslib3D.Vector3D;
import net.java.games.input.Event;
import sage.camera.ICamera;
import sage.input.action.AbstractInputAction;
import sage.input.action.IAction;

public class MyMoveForwardAction extends AbstractInputAction  {

  private ICamera camera;
  private SetSpeedAction runAction;

  public MyMoveForwardAction(ICamera c, SetSpeedAction r) {
    camera = c;
    runAction = r;
  }

  @Override
  public void performAction(float time, Event event) {
    float moveAmount;
    if (runAction.isRunning()) {
      moveAmount = (float) 0.1;
    } else {
      moveAmount = (float) 0.05;
    }
    
  
  Vector3D viewDir = camera.getViewDirection().normalize();
  Vector3D curLocVector = new Vector3D(camera.getLocation());
  Vector3D newLocVector = curLocVector.add(viewDir.mult(moveAmount));
  
 
 
  double newX = newLocVector.getX();
  double newY = newLocVector.getY();
  double newZ = newLocVector.getZ();
  
  Point3D newLoc = new Point3D(newX,newY,newZ);
  camera.setLocation(newLoc);
  
  
  
  }


  
  
  
  
}
