package myGameEngine.input.action;

import graphicslib3D.Point3D;
import graphicslib3D.Vector3D;
import net.java.games.input.Event;
import sage.camera.ICamera;
import sage.input.action.AbstractInputAction;

public class MyMoveRightAction extends AbstractInputAction {

  private ICamera camera;
  private SetSpeedAction runAction;

  public MyMoveRightAction(ICamera c, SetSpeedAction r) {
    camera = c;
    runAction = r;
  }

  @Override
  public void performAction(float time, Event event) {
    float moveAmount;
    if (runAction.isRunning()) {
      moveAmount = (float) 0.05;
    } else {
      moveAmount = (float) 0.05;
    }
  
  
  Vector3D viewDir = camera.getRightAxis().normalize();
  Vector3D curLocVector = new Vector3D(camera.getLocation());
  Vector3D newLocVector = curLocVector.minus(viewDir.mult(moveAmount));
 
 
  double newX = newLocVector.getX();
  double newY = newLocVector.getY();
  double newZ = newLocVector.getZ();
  
  Point3D newLoc = new Point3D(newX,newY,newZ);
  camera.setLocation(newLoc);
  
  
  
  }
  
  
  
  
}
