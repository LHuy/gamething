package myGameEngine.input.action;

import graphicslib3D.Matrix3D;
import graphicslib3D.Vector3D;
import net.java.games.input.Event;
import sage.camera.ICamera;
import sage.input.action.AbstractInputAction;

public class MyLookLeftAction extends AbstractInputAction {

  private float rotationAmt = 0.5f;
  private ICamera camera;

  public MyLookLeftAction(ICamera camera) {
    this.camera = camera;
  }

  @Override
  public void performAction(float time, Event evt) {
    Matrix3D rotationAmt = new Matrix3D();
    Vector3D vd = camera.getViewDirection();
    Vector3D ud = camera.getUpAxis();
    Vector3D rd = camera.getRightAxis();


    rotationAmt.rotate(-.1, ud);
    
    vd = vd.mult(rotationAmt);
    rd = rd.mult(rotationAmt);
    camera.setUpAxis(ud.normalize());
    camera.setRightAxis(rd.normalize());
    camera.setViewDirection(vd.normalize());
    //camera.applySettings();
  }

}
