package myGameEngine.input.action;

import graphicslib3D.Matrix3D;
import graphicslib3D.Vector3D;
import net.java.games.input.Event;
import sage.camera.ICamera;
import sage.input.action.AbstractInputAction;

public class MyRYaxis extends AbstractInputAction{
  private ICamera camera;

  public MyRYaxis(ICamera camera) {
    this.camera = camera;
  }

  @Override
  public void performAction(float time, Event evt) {
    Matrix3D rotationAmt = new Matrix3D();
    Vector3D vd = camera.getViewDirection();
    Vector3D ud = camera.getUpAxis();
    Vector3D rd = camera.getRightAxis();
 /*   
  if(evt.getValue() > .2){
      rotationAmt.rotate(-.1, rd);
    }else if(evt.getValue() < -.2) {
      rotationAmt.rotate(.1, rd);
    }
     */
    if(evt.getValue() > .2){
      rotationAmt.rotate(-.1, rd);
    }else {
      rotationAmt.rotate(.1, rd);
    }
   

    vd = vd.mult(rotationAmt);
    ud = ud.mult(rotationAmt);
    camera.setUpAxis(ud.normalize());
    camera.setViewDirection(vd.normalize());

  }
  
  
  
  
}
