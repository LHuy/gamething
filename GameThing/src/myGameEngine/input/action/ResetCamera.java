package myGameEngine.input.action;

import graphicslib3D.Point3D;
import net.java.games.input.Event;
import sage.camera.ICamera;
import sage.input.action.AbstractInputAction;
import sage.input.action.IAction;

public class ResetCamera extends AbstractInputAction implements IAction {

  ICamera camera;
  public ResetCamera(sage.camera.ICamera c){
    this.camera = c;
    
  }
  @Override
  public void performAction(float arg0, Event arg1) {
   camera.setLocation(new Point3D(1, 1, 20));
  
  }
  
  
  
  
}
