package myGameEngine.input.action;

import net.java.games.input.Event;
import sage.app.AbstractGame;
import sage.input.action.AbstractInputAction;
import sage.input.action.IAction;

public class MyQuitGameAction extends AbstractInputAction implements IAction {
  private AbstractGame game; 
  public MyQuitGameAction(AbstractGame game) {
    this.game = game;
    
  }

  @Override
  public void performAction(float time, Event event) { 
    
    game.setGameOver(true);
  System.exit(0);
  }

}
