package myGameEngine;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import sage.scene.TriMesh;

public class MyDiamond extends TriMesh {
  private static float[] vrts = new float[] { 0, (float) 0.5, 0, (float)-.5, (float)-.5, (float).5, (float).5, (float)-.5, (float).5, (float).5,
    (float)-.5, (float)-.5, (float)-.5, (float)-.5, (float)-.5, 0 ,(float)-1.5,0  };
  private static float[] cl = new float[] { 1,0,0,0 ,    0,10,0,0 ,    0,0,0,10 ,
    0,0,10,0 ,   0,10,0,0  , 0,10,0,0  };
  private static int[] triangles = new int[] { 0, 1, 2,    0, 2, 3,    0, 3, 4,    0, 4,
      1,    1, 4, 2,     4, 3, 2,      1,2,5, 2,3,5,  3,4,5, 4,1,5, };

  public MyDiamond() {
   
    FloatBuffer vertBuf = com.jogamp.common.nio.Buffers
        .newDirectFloatBuffer(vrts);
    FloatBuffer colorBuf = com.jogamp.common.nio.Buffers
        .newDirectFloatBuffer(cl);
    IntBuffer triangleBuf = com.jogamp.common.nio.Buffers
        .newDirectIntBuffer(triangles);
    this.setVertexBuffer(vertBuf);
    this.setColorBuffer(colorBuf);
    this.setIndexBuffer(triangleBuf);
  }
}