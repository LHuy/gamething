package myGameEngine;

import java.nio.FloatBuffer;
import java.util.Random;

import com.jogamp.graph.curve.OutlineShape.VerticesState;

import event.TreasureCrashEvent;
import sage.event.IEventListener;
import sage.event.IGameEvent;
import sage.renderer.AbstractRenderer;
import sage.renderer.IRenderer;
import sage.renderer.RenderContext;
import sage.scene.TriMesh;
import sage.scene.shape.Cube;

public class CustomTreasure extends Cube implements IEventListener {
   Random randomGenerator = new Random();
   float tx = randomGenerator.nextInt(20);
   float ty = randomGenerator.nextInt(20);
   float tz = randomGenerator.nextInt(20);

  Cube treasureChest = new Cube("Tresure Chest");

  @Override
  public boolean handleEvent(IGameEvent arg0) {
     TreasureCrashEvent treasureCrashEvent = (TreasureCrashEvent) arg0; 
     int treasureCount= treasureCrashEvent.getCrashCount();
     

 if(treasureCount < 4 ){
   this.scale(2f ,2f,  2f);
 }
 
   
    return true;
  }
  
  
  public CustomTreasure respawnTreasureChest(CustomTreasure customTreasure , int treasureCollected){
    treasureChest.translate(tx, ty, tz*-1);
    if(treasureCollected < 4 ){
      treasureChest.scale(3f ,3f,  3f);
    }
    return customTreasure; 
  }


  /*
  public String getName(){
    return new String("Treasure chest Crash");
  }
  */
  
}