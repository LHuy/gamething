package myGameEngine.Camera.input;

import sage.input.action.AbstractInputAction;
import net.java.games.input.Event;
import myGameEngine.Camera.Camera3Pcontroller;

public class Cam3pLeft extends AbstractInputAction {
  private Camera3Pcontroller camera;


  public Cam3pLeft(Camera3Pcontroller camera3p) {
    camera = camera3p;

  }

  @Override
  public void performAction(float time, Event e) {

    float distance = (float) -0.3;
    camera.setCameraAzimuth(distance);

  }

}
