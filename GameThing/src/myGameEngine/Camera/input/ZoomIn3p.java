package myGameEngine.Camera.input;

import graphicslib3D.Camera;
import graphicslib3D.Matrix3D;
import graphicslib3D.Point3D;
import graphicslib3D.Vector3D;
import myGameEngine.Camera.Camera3Pcontroller;
import net.java.games.input.Event;
import sage.camera.ICamera;
import sage.input.action.AbstractInputAction;
import sage.scene.SceneNode;

public class ZoomIn3p extends AbstractInputAction {
  private Camera3Pcontroller camera;

  
  public ZoomIn3p(Camera3Pcontroller camera3p) {
    camera =camera3p; 
  }
  
  @Override
  public void performAction(float time, Event e) {
    
    float zoom=(float) -0.1; 
    camera.setCameraElevation(zoom);
  }
  
}