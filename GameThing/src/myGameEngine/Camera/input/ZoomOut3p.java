package myGameEngine.Camera.input;

import sage.input.action.AbstractInputAction;
import myGameEngine.Camera.Camera3Pcontroller;
import net.java.games.input.Event;

public class ZoomOut3p extends AbstractInputAction {

  private Camera3Pcontroller camera;


  public ZoomOut3p(Camera3Pcontroller camera3p) {
    camera = camera3p;
  }

  @Override
  public void performAction(float time, Event e) {

    float zoom = 0.1f;
    camera.setCameraElevation(zoom);

  }

}