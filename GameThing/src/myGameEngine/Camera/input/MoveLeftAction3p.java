package myGameEngine.Camera.input;

import graphicslib3D.Matrix3D;
import graphicslib3D.Vector3D;
import net.java.games.input.Event;
import sage.input.action.AbstractInputAction;
import sage.scene.SceneNode;

public class MoveLeftAction3p extends AbstractInputAction {
  private SceneNode avatar;
  private float speed = 0.05f;
  
  
  public MoveLeftAction3p(SceneNode n) {
    avatar =n; 
  }
  
  @Override
  public void performAction(float time, Event e) {
    Matrix3D rot = avatar.getLocalRotation();
    Vector3D dir = new Vector3D(1,0,0);
    dir = dir.mult(rot);
    dir.scale((double)(speed * time));
    avatar.translate((float)dir.getX(), (float)dir.getY(), (float)dir.getZ());
    
  }
}
