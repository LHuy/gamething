package myGameEngine.Camera;

import net.java.games.input.Event;
import net.java.games.input.Component.Identifier.Axis;
import graphicslib3D.Point3D;
import graphicslib3D.Vector3D;
import sage.camera.ICamera;
import sage.input.IInputManager;
import sage.input.IInputManager.INPUT_ACTION_TYPE;
import sage.input.action.AbstractInputAction;
import sage.input.action.IAction;
import sage.scene.SceneNode;
import sage.util.MathUtils;

public class Camera3Pcontroller {

  private ICamera cam;
  public SceneNode target;
  private float cameraAzimuth;
  private float cameraElevation;
  private float cameraDistanceFromTarget;
  private Point3D targetPos;
  private Vector3D worldUpVec;

  public Camera3Pcontroller(ICamera cam, SceneNode target,
      IInputManager inputMgr, String controllerName) {
    this.cam = cam;
    this.target = target;
    worldUpVec = new Vector3D(0, 1, 0);
    cameraDistanceFromTarget = 5.0f;
    cameraAzimuth = 180;
    cameraElevation = 20.0f;
    update(0.0f);
    setupInput(inputMgr, controllerName);
  }

  private void setupInput(IInputManager im, String cn) {
    IAction orbitAction = new OrbitAroundAction();
    
    im.associateAction(cn, Axis.RX, orbitAction,
        INPUT_ACTION_TYPE.REPEAT_WHILE_DOWN);

  }

  public void update(float f) {
    updateTarget();
    updateCameraPosition();
    cam.lookAt(targetPos, worldUpVec);

  }

  private void updateCameraPosition() {
    double theta = cameraAzimuth;
    double phi = cameraElevation;
    double r = cameraDistanceFromTarget;

    Point3D relativePosition = MathUtils.sphericalToCartesian(theta, phi, r);
    Point3D desiredCameraLoc = relativePosition.add(targetPos);
    cam.setLocation(desiredCameraLoc);
  }

  private void updateTarget() {
    targetPos = new Point3D(target.getWorldTranslation().getCol(3));
  }

  
  
  public float setCameraElevation(float zoom){
    return this.cameraDistanceFromTarget = (cameraDistanceFromTarget + zoom); 
  }
  
  public float setCameraAzimuth(float distance){
    return this.cameraAzimuth = (cameraAzimuth + distance); 
  }
  
  
  
  private class OrbitAroundAction extends AbstractInputAction {

    @Override
    public void performAction(float arg0, Event evt) {
      float rotAmount;
      if (evt.getValue() < -0.2) {
        rotAmount = -0.1f;
      } else {
        if (evt.getValue() > 0.2) {
          rotAmount = 0.1f;
        } else {
          rotAmount = 0.0f;
        }

      }

      cameraAzimuth += rotAmount;
      cameraAzimuth = cameraAzimuth % 360;

    }

  }

}
